﻿using UnityEngine;

public class Buttons : MonoBehaviour
{
    public HealthBarEntity Entity;


    void OnGUI()
    {
        if (GUILayout.Button("SetValue"))
        {
            Entity.TotalHealth = 500;
            Entity.CurrentHealth = 500;
        }
        if (GUILayout.Button("Reduce health"))
        {
            Entity.CurrentHealth -= 50;
        }
    }

}
