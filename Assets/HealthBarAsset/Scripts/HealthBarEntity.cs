﻿using UnityEngine;
using System.Collections;

public class HealthBarEntity : MonoBehaviour
{
    /// <summary>
    /// the y axis heith where the health bar should float above the entity
    /// </summary>
    public float Height;
    /// <summary>
    /// Type of health bar to be dispalyed above the entity
    /// </summary>
    public HealthBarManager.HealthBarTypes Type;

    private float _currentHealth;
    private bool _enabled;

    public float CurrentHealth
    {
        get
        {
            return _currentHealth;
        }
        set
        {
            _currentHealth = value;
            ValuesUpdated();
        }
    }

    private float _totalHealth;

    public float TotalHealth
    {
        get
        {
            return _totalHealth;
        }
        set
        {
            _totalHealth = value;
            ValuesUpdated();
        }
    }

    private float _healthRegenValue;

    public float HealthRegen
    {
        get
        {
            return _healthRegenValue;
        }
        set
        {
            _healthRegenValue = value;
            ValuesUpdated();
        }
    }

    public float HealthRatio { get; private set; }

    void OnEnable()
    {
        //Register self with manager
        HealthBarManager.Instance.RegisterEntity(this);
        _enabled = true;
    }

    void ValuesUpdated()
    {
        HealthRatio = CurrentHealth / TotalHealth;
        if(_enabled)
            HealthBarManager.Instance.ValuesUpdated(this);
    }

    void OnDisable()
    {
        _enabled = false;
        //DeRegister self with manager
        HealthBarManager.Instance.DeRegisterEntity(this);
    }

}
