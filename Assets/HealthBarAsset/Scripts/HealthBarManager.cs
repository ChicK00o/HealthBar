﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class HealthBarManager : MonoBehaviour
{
    //Add Types of different health bar visuals here
    //Creeps health bar style would be different to others
    //heros health bar would be longer
    public enum HealthBarTypes
    {
        Goblin,
        Creep1,
        Creep2,
        HeroArcher,
    }

    public Camera CameraToLookAt;
    public AnimationCurve CurveScaleByHeight;

    [SerializeField]
    private List<HealthBarCollectionData> _listOfHealthBar;
    private Canvas _canvas;
    private Dictionary<HealthBarTypes, GameObject> _healthBarTypeCollection = new Dictionary<HealthBarTypes, GameObject>();
    private Dictionary<HealthBarEntity, HealthBarGUI> _healthBarInstances = new Dictionary<HealthBarEntity, HealthBarGUI>();
    private Quaternion _cachedCameraRotation = Quaternion.identity;

    private static HealthBarManager _instance;
    public static HealthBarManager Instance
    {
        get
        {
            if (_instance != null)
            {
                return _instance;
            }
            Debug.LogError("HealthBarManager should be part of the instanciated scene");
            return null;
        }
    }

    void Awake()
    {
        if (CameraToLookAt == null)
        {
            CameraToLookAt = Camera.main;
        }
        _instance = this;
        DontDestroyOnLoad(this);
        _canvas = gameObject.AddComponent<Canvas>();
        _canvas.renderMode = RenderMode.WorldSpace;
        _canvas.sortingOrder = 100;
        foreach (HealthBarCollectionData data in _listOfHealthBar)
        {
            try
            {
                _healthBarTypeCollection.Add(data.Type, data.Prefab);
            }
            catch (Exception e)
            {
                Debug.LogError("Multiple prefabs defined for type : " + data.Type.ToString() + " Exception is : " + e.Message);
            }
        }
    }

    public void RegisterEntity(HealthBarEntity entity)
    {
        GameObject newHealthBar = GameObject.Instantiate(_healthBarTypeCollection[entity.Type]);
        newHealthBar.transform.localPosition = Vector3.zero;
        newHealthBar.transform.SetParent(transform, false);
        HealthBarGUI gui = newHealthBar.GetComponent<HealthBarGUI>();
        gui.Height = entity.Height;
        gui.CurveScaleByHeight = CurveScaleByHeight;
        _healthBarInstances.Add(entity, gui);
    }

    public void DeRegisterEntity(HealthBarEntity entity)
    {
        Destroy(_healthBarInstances[entity].gameObject);
        _healthBarInstances.Remove(entity);
    }

    public void ValuesUpdated(HealthBarEntity entity)
    {
        _healthBarInstances[entity].ValuesUpdated(entity.CurrentHealth, entity.TotalHealth, entity.HealthRatio,
            entity.HealthRegen);
    }

    /// <summary>
    /// Call this on change in camera height || contiously for transition effect, and then only on change
    /// </summary>
    /// <param name="cameraHeightRatio">value between 0 - 1, 0 - lowest height, 1 - hightest height</param>
    public void SetCameraHeightScale(float cameraHeightRatio)
    {
        foreach (HealthBarGUI gui in _healthBarInstances.Values)
        {
            gui.UpdateScale(cameraHeightRatio);
        }
    }

    #region can be converted in to a enumerator and coroutine call for better per frame performace
    void LateUpdate()
    {
        UpdateMovements();
        UpdateLookAt();
    }

    void UpdateMovements()
    {
        foreach (KeyValuePair<HealthBarEntity, HealthBarGUI> pair in _healthBarInstances)
        {
            pair.Value.SetPosition(pair.Key.transform.position);
        }
    }

    void UpdateLookAt()
    {
        if (_cachedCameraRotation == CameraToLookAt.transform.rotation) return;
        _cachedCameraRotation = CameraToLookAt.transform.rotation;
        foreach (HealthBarGUI gui in _healthBarInstances.Values)
        {
            gui.transform.localRotation = _cachedCameraRotation;
        }
    }
    #endregion can be converted in to a enumerator and coroutine call for better per frame performace
}

[System.Serializable]
public class HealthBarCollectionData
{
    public HealthBarManager.HealthBarTypes Type;
    public GameObject Prefab;
}
