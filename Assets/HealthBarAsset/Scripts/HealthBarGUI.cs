﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class HealthBarGUI : MonoBehaviour
{
    public Image Background;
    public Image HealthForeground;
    public Text CurrentHealth;
    public Text TotalHealth;
    public Text HealthRegen;

    private AnimationCurve _scaleCurve;

    public AnimationCurve CurveScaleByHeight
    {
        set { _scaleCurve = value; }
    }

    private bool _valueChanged;
    private float _currentValue;
    private float _newValue;

    private Vector3 _initialScale;
    private float _height;

    public float Height
    {
        set { _height = value; }
    }

    void Start()
    {
        _initialScale = transform.localScale;
    }

    internal void UpdateScale(float cameraHeightRatio)
    {
        transform.localScale = _scaleCurve.Evaluate(cameraHeightRatio)*_initialScale;
    }

    internal void SetPosition(Vector3 entityPosition)
    {
        transform.localPosition = entityPosition + new Vector3(0, _height, 0);
    }

    internal void ValuesUpdated(float currentHealth, float totalHealth, float healthRatio, float healthRegen)
    {
        CurrentHealth.text = currentHealth.ToString("F1");
        TotalHealth.text = totalHealth.ToString("F1");
        HealthRegen.text = "+" + healthRegen.ToString("F1");
        SetHealthBar(healthRatio);
    }

    void SetHealthBar(float ratio)
    {
        Vector3 scale = HealthForeground.rectTransform.localScale;
        scale.x = ratio;
        HealthForeground.rectTransform.localScale = scale;
    }
}
